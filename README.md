## Banking RRESTful API assignment

Clone the repository and checkout **main** branch <br>
Open the application in IntelliJ and create a new Application configuration with main class RivertechApplication.kt

The API is implemented using SpringBoot and Kotlin. 

For Unit tests I use JUnit5 and mockk library for mocking.

After starting the application, the H2 in memory database is seeded with values from the **resources/data.sql** file.

The DB has 3 tables: *beneficiary*, *account*, *transaction*

### Implementation note:
Although not specified in the assignment, I added one more step of registering an explicit beneficiary (user) and after that being able to create one or more accounts for that beneficiary and have a proper DB model

### API calls

#### Creating a beneficiary
POST /user/register <br>
Body: <br>
{ "name" : "<name>" } <br>
Output:
{ "id":"<user_id>", "name":"<name>" } <br>
BAD_REQUEST error if beneficiary with same name exists

#### Creating an account for a given beneficiary
POST /account/add <br>
Body <br>
{ "userId":<user_id>, "pin":<4 digit pin> }
Returns the created account data <br>
NOT_FOUND error if the user_id does nto exist <br>

#### Deposit amount on given account
POST /transactions/deposit <br>
Body <br>
{ "account_id":<account_id>, "amount":<string amount>, "currency":<currency code (USD)>,"date":<ISO date (2022-01-16)> <br>
Returns the transaction <br>
NOT_FOUND if the account does not exist

#### Withdraw amount from given account
POST /transactions/withdraw <br>
Body <br>
{ "account_id":<account_id>,"pin":<account pin>, "amount":<string amount>, "currency":<currency code (USD)>,"date":<ISO date (2022-01-16)> <br>
Returns the transaction <br>
NOT_FOUND if the account does not exist <br>
BAD_REQUEST if pin is incorrect

#### Transfer amount to given account
POST /transactions/deposit <br>
Body <br>
{ "account_id":<account_id>, "pin":<pin>, "targetAccountNumber":<acc number of the receiver account> "amount":<string amount>, "currency":<currency code (USD)>,"date":<ISO date (2022-01-16)> <br>
Returns the transaction <br>
NOT_FOUND if the account does not exist <br>
NOT_FOUND if the receiver account does not exist <br>
BAD_REQUEST if pin is incorrect

#### Get transactions for account
GET /transactions/<account_id>  <br>
Returns the account and a list of all transactions for the given account <br>
NOT_FOUND if the account does not exist

#### Getting full data for a given beneficiary
GET /user/<user_id> <br>
The output consists of the beneficiary name, their accounts with transactions and balance <br>
NOT_FOUND error if beneficiary with user_id does not exist