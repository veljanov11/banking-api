package com.aveljanov.rivertech

import java.util.regex.Pattern

fun String.matchesNot(regex: String) : Boolean {
    return !Pattern.matches(regex, this)
}


fun Boolean.ifFalse(block: () -> Unit) {
    if (this.not()) {
        block.invoke()
    }
}