package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.ifFalse
import com.aveljanov.rivertech.model.TransactionType
import com.aveljanov.rivertech.model.entities.Transaction
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.model.response.TransactionResponse
import com.aveljanov.rivertech.model.response.TransactionsResponse
import com.aveljanov.rivertech.repositories.AccountRepository
import com.aveljanov.rivertech.repositories.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.util.*

@Service
class TransactionService @Autowired constructor(
    private val transactionRepository: TransactionRepository,
    private val accountRepository: AccountRepository
)  {


     fun deposit(accountId: String, amount: BigDecimal, currency: Currency, date: Date): TransactionResponse {
        return accountRepository.findById(accountId).takeIf { it.isPresent }?.let {
            val transaction = Transaction(it.get(), amount, currency, date, TransactionType.DEPOSIT)
            TransactionResponse.from(it.get().id , transactionRepository.save(transaction))
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

     fun withdraw(
        accountId: String,
        pin: String,
        amount: BigDecimal,
        currency: Currency,
        date: Date
    ): TransactionResponse {
        return accountRepository.findById(accountId).takeIf { it.isPresent }?.let {
            val acc = it.get()
            acc.checkPin(pin).ifFalse { throw ResponseStatusException(HttpStatus.BAD_REQUEST) }
            val transaction = Transaction(acc, amount, currency, date, TransactionType.WITHDRAW)
            TransactionResponse.from(it.get().id , transactionRepository.save(transaction))
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

     fun transfer(
        accountId: String,
        pin: String,
        accountNumber: String,
        amount: BigDecimal,
        currency: Currency,
        date: Date
    ): TransactionResponse {
        return accountRepository.findById(accountId).takeIf { it.isPresent }?.let { sender ->
            val senderAccount = sender.get()
            senderAccount.checkPin(pin).ifFalse { throw ResponseStatusException(HttpStatus.BAD_REQUEST) }
            accountRepository.findByAccountNumber(accountNumber).takeIf { it.isPresent }?.let { reciever ->
                val transaction = Transaction(senderAccount, amount, currency, date, TransactionType.WITHDRAW)
                val transactionTransfer = Transaction(reciever.get(), amount, currency, date, TransactionType.DEPOSIT)
                transactionRepository.saveAll(listOf(transaction, transactionTransfer))
                TransactionResponse.from(senderAccount.id , transaction)
            }
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    fun getTransactionsForAccount(accountId: String): TransactionsResponse {
        return accountRepository.findById(accountId).takeIf { it.isPresent }?.get()?.let { account ->
            val transactions = transactionRepository.findAllByAccountId(accountId).takeIf { it.isPresent }?.get() ?: emptyList<Transaction>()
            TransactionsResponse(account = AccountResponse.fromAccount(account), transactions = transactions.map { TransactionResponse.from(account.id, it) })
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

