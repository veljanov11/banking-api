package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.model.entities.Account
import com.aveljanov.rivertech.model.entities.Beneficiary
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.model.response.BeneficiaryDataResponse
import com.aveljanov.rivertech.model.response.BeneficiaryResponse
import com.aveljanov.rivertech.repositories.AccountRepository
import com.aveljanov.rivertech.repositories.BeneficiaryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.rmi.ServerException
import java.util.*

@Service
class BeneficiaryService @Autowired constructor(
    private val beneficiaryRepository: BeneficiaryRepository) {


    fun addBeneficiary(name: String): BeneficiaryResponse {
        val beneficiary = Beneficiary(name, emptyList())

        beneficiaryRepository.findByName(name).takeIf { it.isPresent }?.let {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        }

        val beneficiarySaved = beneficiaryRepository.save(beneficiary)
        return BeneficiaryResponse.from(beneficiarySaved) ?: throw
        ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    fun getBeneficiaryData(userId: String): BeneficiaryDataResponse {
        return beneficiaryRepository.findById(userId).takeIf { it.isPresent }?.get()?.let { beneficiary ->
            val accounts = getAccountResponses(beneficiary)
            BeneficiaryDataResponse(
                name = beneficiary.name,
                accounts = accounts,
                balance = calculateBalance(beneficiary.accounts),
                getCurrency(beneficiary.accounts)
            )
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    private fun getCurrency(accounts: List<Account>) : Currency {
        return accounts.takeIf { it.isNotEmpty() }?.let {
            it[0].transactions.takeIf { it.isNotEmpty() }?.get(0)?.currency
        } ?: Currency.getInstance(Locale.getDefault())
    }

    private fun calculateBalance(accounts: List<Account>): BigDecimal {
        return accounts.sumOf { it.getBalance() }
    }

    private fun getAccountResponses(beneficiary: Beneficiary) =
        beneficiary.accounts.map { account ->
            AccountResponse.fromAccount(account)
        }

}