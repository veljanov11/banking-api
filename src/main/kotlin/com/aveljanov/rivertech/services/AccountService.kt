package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.matchesNot
import com.aveljanov.rivertech.model.entities.Account
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.repositories.AccountRepository
import com.aveljanov.rivertech.repositories.BeneficiaryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.util.Base64Utils
import org.springframework.web.server.ResponseStatusException
import java.util.regex.Pattern
import kotlin.random.Random

private const val ACCOUNT_NUM_PREFIX = "8009"
private const val ACCOUNT_NUMBER_LENGTH = 12

@Service
class AccountService @Autowired constructor(private val beneficiaryRepository: BeneficiaryRepository,
private val accountRepository: AccountRepository) {

     fun createAccount(userId: String, pin: String) : AccountResponse {

        validatePin(pin)

        return beneficiaryRepository.findById(userId).takeIf { it.isPresent }?.let {
            val beneficiary = it.get()
            val account = accountRepository.save(Account(beneficiary = beneficiary, pin = pin, accountNumber = generateAccountNumber(beneficiary.id), transactions = emptyList()))
            AccountResponse.fromAccount(account)
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    private fun validatePin(pin: String) {
        pin.matchesNot("[0-9]{4}").takeIf { it }?.let {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        }
    }

    private fun generateAccountNumber(userId: String) : String {
        val random = Random(userId.hashCode() + Random.nextInt())
        return (1..ACCOUNT_NUMBER_LENGTH).map {
            random.nextInt(0, 9)
        }.joinToString(separator = "", prefix = ACCOUNT_NUM_PREFIX)
    }
}

