package com.aveljanov.rivertech.model

enum class TransactionType {
    DEPOSIT,
    WITHDRAW
}