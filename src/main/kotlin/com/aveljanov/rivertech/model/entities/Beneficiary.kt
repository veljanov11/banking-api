package com.aveljanov.rivertech.model.entities

import javax.persistence.*

@Entity
data class Beneficiary(
    @Column
    val name: String,
    @OneToMany(mappedBy = "beneficiary", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val accounts: List<Account>
) : BaseEntity()
