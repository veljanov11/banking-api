package com.aveljanov.rivertech.model.entities

import com.aveljanov.rivertech.model.TransactionType
import java.math.BigDecimal
import javax.persistence.*

@Entity
data class Account(
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "beneficiaryId", nullable = false)
    val beneficiary: Beneficiary,
    @Column
    val pin: String,
    @Column
    val accountNumber: String,
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val transactions: List<Transaction>
) : BaseEntity() {
    fun checkPin(pin: String) : Boolean
        = this.pin == pin

    fun getBalance() : BigDecimal {
        return transactions.sumOf {
            when(it.type) {
                TransactionType.DEPOSIT -> it.amount
                TransactionType.WITHDRAW -> it.amount.negate()
            }
        }
    }
}
