package com.aveljanov.rivertech.model.entities

import com.aveljanov.rivertech.model.TransactionType
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
data class Transaction(
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "accountId", nullable = false)
    val account: Account,
    @Column
    val amount: BigDecimal,
    @Column
    val currency: Currency,
    @Column
    val date: Date,
    @Column
    val type: TransactionType
) : BaseEntity()
