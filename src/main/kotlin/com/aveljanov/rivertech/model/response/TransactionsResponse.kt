package com.aveljanov.rivertech.model.response

data class TransactionsResponse(
    val account: AccountResponse,
    val transactions: List<TransactionResponse>
)
