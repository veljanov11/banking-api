package com.aveljanov.rivertech.model.response

import com.aveljanov.rivertech.model.entities.Beneficiary

data class BeneficiaryResponse(
    val userId: String,
    val name: String,
) {
    companion object {
        fun from(beneficiary: Beneficiary) : BeneficiaryResponse? {
            return beneficiary.id?.let {
                BeneficiaryResponse(beneficiary.id, beneficiary.name)
            }
        }
    }
}
