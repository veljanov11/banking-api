package com.aveljanov.rivertech.model.response

import com.aveljanov.rivertech.model.TransactionType
import com.aveljanov.rivertech.model.entities.Transaction
import java.math.BigDecimal
import java.util.*

data class TransactionResponse(
    val transactionId: String,
    val accountId: String,
    val type: TransactionType,
    val amount: BigDecimal,
    val currency: Currency,
    val date: Date
) {
    companion object {
        fun from(accountId: String, transaction: Transaction) =
            TransactionResponse(accountId = accountId, transactionId = transaction.id, type = transaction.type, amount = transaction.amount,
            currency = transaction.currency, date = transaction.date)
    }
}
