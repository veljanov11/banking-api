package com.aveljanov.rivertech.model.response

import com.aveljanov.rivertech.model.entities.Account

data class AccountResponse(
    val accountId: String,
    val userId: String,
    val accountNumber: String,
    val transactions: List<TransactionResponse>
) {
    companion object {
        fun fromAccount(account: Account) : AccountResponse {
            return AccountResponse(accountId = account.id, accountNumber = account.accountNumber, userId = account.beneficiary.id,
                transactions = account.transactions.map { TransactionResponse.from(account.id, it) }
                )
        }
    }
}
