package com.aveljanov.rivertech.model.response

import java.math.BigDecimal
import java.util.*

data class BeneficiaryDataResponse (
    val name: String,
    val accounts: List<AccountResponse>,
    val balance: BigDecimal,
    val currency: Currency
)
