package com.aveljanov.rivertech.model.request

data class AddUserRequest(
    val name: String
)
