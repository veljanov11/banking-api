package com.aveljanov.rivertech.model.request

data class AddAccountRequest(
    val userId: String,
    val pin: String,
)
