package com.aveljanov.rivertech.model.request

import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.util.*

data class TransferRequest(
    override val amount: BigDecimal,
    override val currency: String,
    @DateTimeFormat(pattern = "yyyy/mm/dd")
    override val date: Date,
    override val pin: String,
    val targetAccountNumber: String,
    override val accountId: String
) : PinTransactionRequest

data class WithdrawRequest(
    override val amount: BigDecimal,
    override val currency: String,
    @DateTimeFormat(pattern = "yyyy/mm/dd")
    override val date: Date,
    override val pin: String,
    override val accountId: String
) : PinTransactionRequest

data class DepositRequest(
    override val amount: BigDecimal,
    override val currency: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    override val date: Date,
    override val accountId: String
) : TransactionRequest

interface PinTransactionRequest : TransactionRequest {
    val pin: String
}

interface TransactionRequest {
    val accountId: String
    val amount: BigDecimal
    val currency: String
    val date: Date
}
