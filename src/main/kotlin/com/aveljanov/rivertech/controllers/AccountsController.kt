package com.aveljanov.rivertech.controllers

import com.aveljanov.rivertech.model.request.AddAccountRequest
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.services.AccountService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@RestController
class AccountsController (private val accountService: AccountService) {

    @PostMapping("/account/add")
    fun addAccount(@RequestBody addAccountRequest: AddAccountRequest) : AccountResponse {
        return accountService.createAccount(userId = addAccountRequest.userId, pin = addAccountRequest.pin)
    }

}