package com.aveljanov.rivertech.controllers

import com.aveljanov.rivertech.model.request.DepositRequest
import com.aveljanov.rivertech.model.request.TransferRequest
import com.aveljanov.rivertech.model.request.WithdrawRequest
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.model.response.TransactionResponse
import com.aveljanov.rivertech.model.response.TransactionsResponse
import com.aveljanov.rivertech.services.TransactionService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.Collections.emptyList
import javax.websocket.server.PathParam

@RestController
class TransactionController(private val transactionService: TransactionService) {

    @PostMapping("/transactions/deposit")
    fun deposit(@RequestBody depositRequest: DepositRequest): TransactionResponse {
        return transactionService.deposit(
            accountId = depositRequest.accountId,
            amount = depositRequest.amount,
            currency = Currency.getInstance(depositRequest.currency),
            date = depositRequest.date
        )
    }


    @PostMapping("/transactions/withdraw")
    fun withdraw(@RequestBody withdrawRequest: WithdrawRequest): TransactionResponse {
        return transactionService.withdraw(
            accountId = withdrawRequest.accountId,
            pin = withdrawRequest.pin,
            amount = withdrawRequest.amount,
            currency = Currency.getInstance(withdrawRequest.currency),
            date = withdrawRequest.date
        )
    }

    @PostMapping("/transactions/transfer")
    fun transfer(@RequestBody transferRequest: TransferRequest): TransactionResponse {
        return transactionService.transfer(
            accountId = transferRequest.accountId,
            pin = transferRequest.pin,
            amount = transferRequest.amount,
            currency = Currency.getInstance(transferRequest.currency),
            date = transferRequest.date,
            accountNumber = transferRequest.targetAccountNumber
        )
    }

    @GetMapping("/transactions/{accountId}")
    fun getTransactionsForAccount(@PathVariable("accountId") accountId: String): TransactionsResponse {
        return transactionService.getTransactionsForAccount(accountId)
    }

}