package com.aveljanov.rivertech.controllers

import com.aveljanov.rivertech.model.request.AddUserRequest
import com.aveljanov.rivertech.model.response.BeneficiaryDataResponse
import com.aveljanov.rivertech.model.response.BeneficiaryResponse
import com.aveljanov.rivertech.services.BeneficiaryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.util.*
import java.util.Collections.emptyList
import javax.websocket.server.PathParam

@RestController
class BeneficiaryController @Autowired constructor(
    val beneficiaryService: BeneficiaryService
) {
    @PostMapping("/user/register")
    fun addBeneficiary(@RequestBody addUserRequest: AddUserRequest) : BeneficiaryResponse {
        return beneficiaryService.addBeneficiary(addUserRequest.name)
    }

    @GetMapping("/user/{userId}")
    fun getBeneficiaryData(@PathVariable("userId") userId: String) : BeneficiaryDataResponse {
        return beneficiaryService.getBeneficiaryData(userId)
    }

}

