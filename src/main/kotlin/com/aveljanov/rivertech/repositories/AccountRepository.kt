package com.aveljanov.rivertech.repositories

import com.aveljanov.rivertech.model.entities.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AccountRepository : JpaRepository<Account, String> {

    fun findByAccountNumber(accountNumber: String) : Optional<Account>

}