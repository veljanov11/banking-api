package com.aveljanov.rivertech.repositories

import com.aveljanov.rivertech.model.entities.Transaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface TransactionRepository : JpaRepository<Transaction, String> {
    fun findAllByAccountId(accountId: String) : Optional<List<Transaction>>
}