package com.aveljanov.rivertech.repositories

import com.aveljanov.rivertech.model.entities.Beneficiary
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BeneficiaryRepository : JpaRepository<Beneficiary, String> {
    fun findByName(name: String) : Optional<Beneficiary>
}