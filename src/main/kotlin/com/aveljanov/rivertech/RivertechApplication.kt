package com.aveljanov.rivertech

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RivertechApplication

fun main(args: Array<String>) {
	runApplication<RivertechApplication>(*args)
}
