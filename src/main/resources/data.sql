INSERT INTO beneficiary (id, name) VALUES ('1', 'Aleksandar')
INSERT INTO account (id, pin, account_number, beneficiary_id) VALUES ('1', '1234', '1234123412341234', '1')
INSERT INTO account (id, pin, account_number, beneficiary_id) VALUES ('2', '2345', '2345234523452345', '1')
INSERT INTO transaction (id, amount, currency, date, type, account_id) VALUES ('1', '1000', 'USD', '2022-01-16', 0, '1')
INSERT INTO transaction (id, amount, currency, date, type, account_id) VALUES ('2', '1000', 'USD', '2022-01-16', 0, '1')
INSERT INTO transaction (id, amount, currency, date, type, account_id) VALUES ('3', '500', 'USD', '2022-01-16', 1, '1')
INSERT INTO transaction (id, amount, currency, date, type, account_id) VALUES ('4', '2000', 'USD', '2022-01-16', 0, '2')

