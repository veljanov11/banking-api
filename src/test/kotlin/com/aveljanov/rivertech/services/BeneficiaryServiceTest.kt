package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.model.TransactionType
import com.aveljanov.rivertech.model.entities.Account
import com.aveljanov.rivertech.model.entities.Beneficiary
import com.aveljanov.rivertech.model.entities.Transaction
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.model.response.BeneficiaryDataResponse
import com.aveljanov.rivertech.model.response.BeneficiaryResponse
import com.aveljanov.rivertech.repositories.BeneficiaryRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.http.HttpStatus
import org.springframework.test.context.transaction.TestTransaction
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.util.*

internal class BeneficiaryServiceTest {

    private lateinit var beneficiaryService: BeneficiaryService

    private val beneficiaryRepository = mockk<BeneficiaryRepository>(relaxed = true)

    private lateinit var testBeneficiary: Beneficiary


    @BeforeEach
    fun setUp() {
        beneficiaryService = BeneficiaryService(beneficiaryRepository)
    }

    @AfterEach
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun addBeneficiary() {
        val name = "Aleksandar"
        val beneficiary = Beneficiary(name, emptyList())
        val response = BeneficiaryResponse.from(beneficiary)
        every { beneficiaryRepository.save(beneficiary) } returns beneficiary
        assertEquals(response, beneficiaryService.addBeneficiary(name))
    }

    @Test
    fun addBeneficiary_Duplicate() {
        val name = "Aleksandar"
        val beneficiary = Beneficiary(name, emptyList())
        every { beneficiaryRepository.findByName(name) } returns Optional.of(beneficiary)
        val ex = assertThrows(ResponseStatusException::class.java) { beneficiaryService.addBeneficiary(name) }
        assertEquals(HttpStatus.BAD_REQUEST, ex.status)
    }

}