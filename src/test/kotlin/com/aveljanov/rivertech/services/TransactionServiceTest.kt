package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.model.TransactionType
import com.aveljanov.rivertech.model.entities.Account
import com.aveljanov.rivertech.model.entities.Beneficiary
import com.aveljanov.rivertech.model.entities.Transaction
import com.aveljanov.rivertech.model.response.TransactionResponse
import com.aveljanov.rivertech.repositories.AccountRepository
import com.aveljanov.rivertech.repositories.TransactionRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.util.*

internal class TransactionServiceTest {

    private lateinit var transactionService: TransactionService

    private val transactionRepository = mockk<TransactionRepository>(relaxed = true)
    private val accountRepository = mockk<AccountRepository>(relaxed = true)

    @BeforeEach
    fun setUp() {
        transactionService = TransactionService(transactionRepository, accountRepository)
    }

    @AfterEach
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun deposit_AccountNotFound() {
        every { accountRepository.findById(any()) } returns Optional.empty()
        val ex = assertThrows(ResponseStatusException::class.java) {
            transactionService.deposit("1", BigDecimal(1000), Currency.getInstance("USD"), Date())
        }
        assertEquals(HttpStatus.NOT_FOUND, ex.status)
    }

    @Test
    fun deposit_Success() {
        val account = Account(Beneficiary("Aleksandar", emptyList()), "1234", "123412341234", emptyList())
        val deposit = Transaction(account, BigDecimal(1000), Currency.getInstance("USD"), Date(), TransactionType.DEPOSIT)
        every { accountRepository.findById(any()) } returns Optional.of(account)
        every { transactionRepository.save(any()) } returns deposit
        val actual = transactionService.deposit("1", BigDecimal(1000), Currency.getInstance("USD"), Date())
        assertEquals(TransactionType.DEPOSIT, actual.type)
        assertEquals(BigDecimal(1000), actual.amount)
    }

    @Test
    fun withdraw_AccountNotFound() {
        every { accountRepository.findById(any()) } returns Optional.empty()
        val ex = assertThrows(ResponseStatusException::class.java) {
            transactionService.withdraw("1", "1234", BigDecimal(1000), Currency.getInstance("USD"), Date())
        }
        assertEquals(HttpStatus.NOT_FOUND, ex.status)
    }

    @Test
    fun withdraw_Success() {
        val account = Account(Beneficiary("Aleksandar", emptyList()), "1234", "123412341234", emptyList())
        val withdraw = Transaction(account, BigDecimal(1000), Currency.getInstance("USD"), Date(), TransactionType.WITHDRAW)
        every { accountRepository.findById(any()) } returns Optional.of(account)
        every { transactionRepository.save(any()) } returns withdraw
        val actual = transactionService.withdraw("1", "1234", BigDecimal(1000), Currency.getInstance("USD"), Date())
        assertEquals(TransactionType.WITHDRAW, actual.type)
        assertEquals(BigDecimal(1000), actual.amount)
    }

    @Test
    fun transfer_SenderNotFound() {
        every { accountRepository.findById(any()) } returns Optional.empty()
        val ex = assertThrows(ResponseStatusException::class.java) {
            transactionService.transfer("1", "1234", "1231432432432", BigDecimal(1000), Currency.getInstance("USD"), Date())
        }
        assertEquals(HttpStatus.NOT_FOUND, ex.status)
    }

    @Test
    fun transfer_ReceiverNotFound() {
        val account = Account(Beneficiary("Aleksandar", emptyList()), "1234", "123412341234", emptyList())
        every { accountRepository.findById(any()) } returns Optional.of(account)
        every { accountRepository.findByAccountNumber(any()) } returns Optional.empty()
        val ex = assertThrows(ResponseStatusException::class.java) {
            transactionService.transfer("1", "1234", "1231432432432", BigDecimal(1000), Currency.getInstance("USD"), Date())
        }
        assertEquals(HttpStatus.NOT_FOUND, ex.status)
    }

    @Test
    fun transfer_Success() {
        val account = Account(Beneficiary("Aleksandar", emptyList()), "1234", "123412341234", emptyList())
        val account2 = Account(Beneficiary("Aleksandar", emptyList()), "1234", "2345234523452345", emptyList())
        val transfer = Transaction(account, BigDecimal(1000), Currency.getInstance("USD"), Date(), TransactionType.WITHDRAW)
        every { accountRepository.findById(any()) } returns Optional.of(account)
        every { accountRepository.findByAccountNumber("2345234523452345") } returns Optional.of(account2)
        every { transactionRepository.saveAll<Transaction>(any()) } returns listOf(transfer)
        val actual = transactionService.transfer(
            "1",
            "1234",
            "2345234523452345",
            BigDecimal(1000),
            Currency.getInstance("USD"),
            Date()
        )
        assertEquals(TransactionType.WITHDRAW, actual.type)
        assertEquals(BigDecimal(1000), actual.amount)
    }

    @Test
    fun getTransactionsForAccount() {
    }
}