package com.aveljanov.rivertech.services

import com.aveljanov.rivertech.model.entities.Account
import com.aveljanov.rivertech.model.entities.Beneficiary
import com.aveljanov.rivertech.model.response.AccountResponse
import com.aveljanov.rivertech.repositories.AccountRepository
import com.aveljanov.rivertech.repositories.BeneficiaryRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.util.*

internal class AccountServiceTest {

    private lateinit var accountService: AccountService

    private val beneficiaryRepository = mockk<BeneficiaryRepository>(relaxed = true)
    private val accountRepository = mockk<AccountRepository>(relaxed = true)

    @BeforeEach
    fun setUp() {
        accountService = AccountService(beneficiaryRepository, accountRepository)
    }

    @AfterEach
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun createAccount_Success() {
        val userId = "1"
        val beneficiary = Beneficiary("Aleksandar", emptyList())
        val account = Account(beneficiary, "1234", "1234123412341234", emptyList())
        every { beneficiaryRepository.findById(userId) } returns Optional.of(beneficiary)
        every { accountRepository.save(any()) } returns account

        assertEquals(AccountResponse.fromAccount(account), accountService.createAccount(userId, "1234"))
    }

    @Test
    fun createAccount_BeneficiaryNotFound() {
        val userId = "1"
        val beneficiary = Beneficiary("Aleksandar", emptyList())
        val account = Account(beneficiary, "1234", "1234123412341234", emptyList())
        every { beneficiaryRepository.findById(userId) } returns Optional.empty()
        every { accountRepository.save(account) } returns account

        val ex = assertThrows(ResponseStatusException::class.java) { accountService.createAccount(userId, "1234") }
        assertEquals(HttpStatus.NOT_FOUND, ex.status)
    }

    @Test
    fun createAccount_PinInvalid() {
        val userId = "1"
        val beneficiary = Beneficiary("Aleksandar", emptyList())
        val account = Account(beneficiary, "12345", "1234123412341234", emptyList())
        every { beneficiaryRepository.findById(userId) } returns Optional.of(beneficiary)
        every { accountRepository.save(account) } returns account

        val ex = assertThrows(ResponseStatusException::class.java) { accountService.createAccount(userId, "12345") }
        assertEquals(HttpStatus.BAD_REQUEST, ex.status)
    }
}